import { useRouter } from "next/router";
import { useState, useRef, useEffect } from "react";
import { Modal } from "semantic-ui-react";

export const ModalCountDown = ({ text, redirectUrl }) => {
  const [countDown, setCountDown] = useState(5);
  const delay = 1;
  const timer = useRef(null);
  useEffect(() => {
    timer.current = setInterval(() => setCountDown((v) => v - 1), delay * 1000);
    return () => {
      clearInterval(timer.current);
    };
  }, []);

  return (
    <Modal
      onClose={() => null}
      onOpen={() => null}
      open={true}
      style={{ textAlign: "center" }}
    >
      <Modal.Header>{text}</Modal.Header>
      <Modal.Content image>
        <Modal.Description>
          <h1>{countDown}</h1>
          <Child
            counter={countDown}
            currentTimer={timer.current}
            redirectUrl={redirectUrl}
          />
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
};

function Child({ counter, currentTimer, redirectUrl }) {
  const router = useRouter();
  useEffect(() => {
    if (counter > 0) return;
    clearInterval(currentTimer);
    router.push(redirectUrl);
  }, [counter, currentTimer]);
  return null;
}
