import { dbConnect } from "utils/mongoose";
import Challenge from "models/Challenge";

dbConnect();

export default async function handler(req, res) {
  let { method, body } = req;
  switch (method) {
    case "GET":
      const challenges = await Challenge.find();
      return res.status(200).json(challenges);
      break;

    case "POST":
      const newChallenge = new Challenge(body);
      const savedChallenge = await newChallenge.save();
      return res.status(201).json(savedChallenge);
      break;

    default:
      return res.status(400).json({ msg: "This method is not supported" });
      break;
  }
}
