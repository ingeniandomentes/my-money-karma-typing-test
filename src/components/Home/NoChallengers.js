import Link from "next/link";
import { Grid, Button } from "semantic-ui-react";

export const NoChallengers = () => {
  return (
    <Grid
      centered
      verticalAlign="middle"
      columns="1"
      style={{ height: "80vh", marginTop: "2rem" }}
    >
      <Grid.Row>
        <Grid.Column textAlign="center">
          <h1>There are not challengers yet</h1>
          <img
            src="https://image.freepik.com/vector-gratis/ilustracion-concepto-vacio_114360-1233.jpg"
            alt="Image not challengers"
            width="20%"
          />
          <div>
            <Link href="/challenges/new">
              <Button primary>Be the first one</Button>
            </Link>
          </div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};
