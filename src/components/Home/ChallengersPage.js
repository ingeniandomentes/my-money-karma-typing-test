import { Divider, Grid } from "semantic-ui-react";
import { HeaderChallengers } from "./HeaderChallengers";
import { ListChallengers } from "./ListChallengers";
import { NoChallengers } from "./NoChallengers";

export const ChallengersPage = ({ challenges }) => {
  return (
    <>
      <Grid columns={2} relaxed="very">
        <Grid.Row>
          <Grid.Column verticalAlign="middle">
            <HeaderChallengers />
          </Grid.Column>
          <Grid.Column verticalAlign="middle">
            {challenges.length !== 0 && (
              <ListChallengers challenges={challenges} />
            )}
            {challenges.length === 0 && <NoChallengers />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Divider vertical>MMK</Divider>
    </>
  );
};
