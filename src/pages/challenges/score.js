import { useEffect, useState } from "react";
import {
  Container,
  Header,
  Icon,
  List,
  Image,
  Grid,
  Button,
} from "semantic-ui-react";
import Cookies from "universal-cookie";
import fetch from "isomorphic-fetch";
import Link from "next/link";

export default function Score() {
  const cookies = new Cookies();
  const [initialValues, setInitialValues] = useState({
    userName: cookies.get("userName"),
    userEmail: cookies.get("userEmail"),
    durationTime: cookies.get("durationTime"),
    durationTimeMinutes: parseInt(cookies.get("durationTime")) / 60,
    textSelected: cookies.get("textSelected"),
    textLength: cookies.get("textSelected")?.length,
    score: cookies.get("score"),
    speed: cookies.get("speed"),
    badScore: cookies.get("badScore"),
    finalTime:
      Math.round((parseInt(cookies.get("finalTime")) / 60) * 100) / 100,
    scoreTotal: cookies.get("scoreTotal"),
  });

  useEffect(() => {
    if (!cookies.get("userName")) {
      router.push("/challenges/new");
    } else {
      submitScore();
    }
  }, []);

  const submitScore = async () => {
    const body = {
      userName: cookies.get("userName"),
      userEmail: cookies.get("userEmail"),
      durationTime: parseInt(cookies.get("durationTime")),
      textSelected: cookies.get("textSelected"),
      textLength: cookies.get("textSelected")?.length,
      score: parseInt(cookies.get("score")),
      speed: parseInt(cookies.get("finalTime")),
    };
    try {
      await fetch(`${process.env.NEXT_PUBLIC_API_URL}challenges`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      }).then(() => {
        cookies.remove("userName");
        cookies.remove("userEmail");
        cookies.remove("durationTime");
        cookies.remove("textSelected");
        cookies.remove("score");
        cookies.remove("finalTime");
        cookies.remove("scoreTotal");
        cookies.remove("badScore");
      });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <Container style={{ marginTop: "2rem" }}>
        <div>
          <Header as="h2" icon textAlign="center">
            <Icon name="sun" circular />
            <Header.Content>Your score is:</Header.Content>
          </Header>
        </div>
        <div>
          <Header as="h1" icon textAlign="center">
            <Header.Content>
              {initialValues?.score} / {initialValues?.scoreTotal}
            </Header.Content>
          </Header>
        </div>
        <Header as="h1" textAlign="center">
          Summary
        </Header>
        <Grid textAlign="center" style={{ marginTop: "15px" }}>
          <List animated verticalAlign="middle">
            <List.Item>
              <Icon name="point" circular />
              <List.Content>
                <List.Header as="a">Time: </List.Header>
                <List.Description>
                  {initialValues?.finalTime} /{" "}
                  {initialValues?.durationTimeMinutes} minute(s)
                </List.Description>
              </List.Content>
            </List.Item>
            <List.Item>
              <Icon name="point" circular />
              <List.Content>
                <List.Header as="a">Wrong words: </List.Header>
                <List.Description>
                  {initialValues?.badScore} / {initialValues?.scoreTotal}{" "}
                  word(s)
                </List.Description>
              </List.Content>
            </List.Item>
          </List>
        </Grid>
        <Grid columns={2} style={{ marginTop: "10px" }} textAlign="center">
          <Grid.Row>
            <Grid.Column>
              <Link href="/challenges/new">
                <Button primary>New Challenge</Button>
              </Link>
            </Grid.Column>
            <Grid.Column>
              <Link href="/">
                <Button color="green">Back Home</Button>
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </>
  );
}
