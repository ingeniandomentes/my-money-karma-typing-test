import { Layout } from "components/Layout";
import SnackbarProvider from "react-simple-snackbar";
import "semantic-ui-css/semantic.min.css";
import "../styles/globals.css";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <SnackbarProvider>
      <Head>
        <title>My Money Karma</title>
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SnackbarProvider>
  );
}

export default MyApp;
