import { Schema, model, models } from "mongoose";

const challengeSchema = new Schema(
  {
    userName: {
      type: String,
      required: [true, "User name is required"],
      trim: true,
    },
    userEmail: {
      type: String,
      required: [true, "User email is required"],
      trim: true,
    },
    durationTime: {
      type: Number,
      required: [true, "Duration time is required"],
    },
    textSelected: {
      type: String,
      required: true,
      trim: true,
      maxlength: [500, "Text must be less than 500 characters"],
    },
    textLength: {
      type: Number,
      required: true,
    },
    score: {
      type: Number,
      required: [true, "Score is required"],
    },
    speed: {
      type: Number,
      required: [true, "Speed is required"],
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default models.Challenge || model("Challenge", challengeSchema);
