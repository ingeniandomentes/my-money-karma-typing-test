import {
  Button,
  Form,
  Grid,
  Popup,
  Icon,
  Dropdown,
  Modal,
} from "semantic-ui-react";
import { useCallback, useState } from "react";
import { LoremIpsum } from "lorem-ipsum";
import Cookies from "universal-cookie";
import { useRouter } from "next/router";
import { ModalCountDown } from "components/Challenges/ModalCountDown";

export default function ChallengeFormPage() {
  const cookies = new Cookies();
  const router = new useRouter();

  const [newChallenge, setNewChallenge] = useState({
    userName: "",
    userEmail: "",
    durationTime: null,
    durationTimeCustom: null,
    textSelected: "",
  });
  const [errors, setErrors] = useState({});
  const [openModal, setOpenModal] = useState(false);

  const timeOptions = [
    { key: "0", value: "1", text: "1m" },
    { key: "1", value: "2", text: "2m" },
    { key: "2", value: "5", text: "5m" },
    { key: "3", value: "3", text: "Custom" },
  ];

  const handleSubmit = async (e) => {
    e.preventDefault();
    cookies.remove("userName");
    cookies.remove("userEmail");
    cookies.remove("durationTime");
    cookies.remove("textSelected");
    let errors = await handleValidate();
    if (Object.keys(errors).length) {
      setErrors(errors);
    } else {
      cookies.set("userName", newChallenge?.userName);
      cookies.set("userEmail", newChallenge?.userEmail);
      cookies.set(
        "durationTime",
        newChallenge?.durationTime !== "3"
          ? parseInt(newChallenge?.durationTime) * 60
          : parseInt(newChallenge?.durationTimeCustom) * 60
      );
      cookies.set("textSelected", newChallenge?.textSelected);
      setOpenModal(true);
      handleCountDown();
    }
  };

  const handleCountDown = () => {
    setOpenModal(true);
  };

  const handleCreateRandomText = async (e) => {
    e.preventDefault();
    const lorem = new LoremIpsum({
      sentencesPerParagraph: {
        max: 8,
        min: 4,
      },
      wordsPerSentence: {
        max: 16,
        min: 4,
      },
    });
    const min = 1;
    const max = 500;
    const rand = Math.round(min + Math.random() * (max - min));
    const generatedText = lorem.generateWords(rand);
    setNewChallenge({ ...newChallenge, textSelected: generatedText });
  };

  const handleChange = (e) =>
    setNewChallenge({ ...newChallenge, [e.target.name]: e.target.value });

  const handleChangeTime = (e, { value }) =>
    setNewChallenge({ ...newChallenge, durationTime: value });

  const handleValidate = async () => {
    await setErrors({});
    const errors = {};
    if (!newChallenge?.userName) errors.userName = "Full Name is required";
    if (!newChallenge?.userEmail) errors.userEmail = "Email is required";
    if (!newChallenge?.durationTime)
      errors.durationTime = "Duration Time is required";
    if (newChallenge?.durationTime === "3" && !newChallenge?.durationTimeCustom)
      errors.durationTimeCustom = "Custom Duration Time is required";
    if (!newChallenge?.textSelected) errors.textSelected = "Text is required";
    return errors;
  };

  return (
    <>
      <Grid
        centered
        verticalAlign="middle"
        columns="3"
        style={{ height: "80vh", marginTop: "2rem" }}
      >
        <Grid.Row columns={2}>
          <Grid.Column textAlign="center">
            <h1>
              New Challenger{" "}
              <Popup
                content="Fill the data to start the game!"
                trigger={<Icon circular name="info" size="tiny" />}
                inverted
              />
            </h1>

            <Form onSubmit={handleSubmit}>
              <Form.Input
                label="Full Name"
                placeholder="Full Name"
                name="userName"
                onChange={handleChange}
                error={
                  errors.userName
                    ? { content: errors.userName, pointing: "below" }
                    : null
                }
              />
              <Form.Input
                label="Email"
                placeholder="Email"
                type="email"
                name="userEmail"
                onChange={handleChange}
                error={
                  errors.userEmail
                    ? { content: errors.userEmail, pointing: "below" }
                    : null
                }
              />
              <Form.TextArea
                label="Text"
                placeholder="Text"
                name="textSelected"
                onChange={handleChange}
                value={newChallenge?.textSelected}
                error={
                  errors.textSelected
                    ? { content: errors.textSelected, pointing: "below" }
                    : null
                }
              />
              <Button secondary onClick={handleCreateRandomText}>
                Generate random text
              </Button>
              <Grid columns={2} style={{ margin: "0 0 1em" }}>
                <Grid.Row>
                  <Grid.Column style={{ paddingLeft: "0" }}>
                    <label forhtml="durationTime">Duration Time</label>
                    <Dropdown
                      placeholder="Select Duration Time"
                      fluid
                      selection
                      options={timeOptions}
                      onChange={handleChangeTime}
                      error={errors.durationTime ? true : false}
                    />
                  </Grid.Column>
                  <Grid.Column style={{ paddingRight: "0" }}>
                    <Form.Input
                      placeholder="Custom time"
                      label="Custom Time"
                      name="durationTimeCustom"
                      type="number"
                      min={1}
                      onChange={handleChange}
                      disabled={
                        newChallenge?.durationTime === "3" ? false : true
                      }
                      error={
                        errors.durationTimeCustom
                          ? {
                              content: errors.durationTimeCustom,
                              pointing: "below",
                            }
                          : null
                      }
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Button primary>Start challenge</Button>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      {openModal && (
        <ModalCountDown
          text={"Your test start in:"}
          redirectUrl={"/challenges/challenge"}
        />
      )}
    </>
  );
}
