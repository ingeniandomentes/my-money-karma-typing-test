import { ChallengersPage } from "components/Home/ChallengersPage";

export default function HomePage({ challenges }) {
  return <ChallengersPage challenges={challenges} />;
}

export const getServerSideProps = async (ctx) => {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URL + "challenges");
  const challenges = await res.json();
  return {
    props: {
      challenges,
    },
  };
};
