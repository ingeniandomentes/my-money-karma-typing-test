import { useRouter } from "next/router";
import { useEffect, useState, useRef } from "react";
import {
  Container,
  Grid,
  Header,
  Icon,
  Form,
  TextArea,
  Divider,
} from "semantic-ui-react";
import Cookies from "universal-cookie";
import { useSnackbar } from "react-simple-snackbar";
import { ModalCountDown } from "components/Challenges/ModalCountDown";

export default function ChallengesPage() {
  const cookies = new Cookies();
  const router = new useRouter();
  const [openSnack, closeSnack] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "green", marginTop: "2rem" },
  });
  const [openSnackErr, closeSnackErr] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "red", marginTop: "2rem" },
  });

  const [initialValues, setInitialValues] = useState({
    userName: "",
    durationTime: 0,
    durationTimeSeconds: 0,
    textSelected: "",
    splitedTextSelected: [],
    textTyped: "",
    textTypedSplited: [],
    score: 0,
    badScore: 0,
  });

  const [openModal, setOpenModal] = useState(false);
  const [countDown, setCountDown] = useState(
    parseInt(cookies.get("durationTime"))
  );

  const timer = useRef(null);
  const delay = 1;
  useEffect(() => {
    if (!cookies.get("userName")) {
      router.push("/challenges/new");
    } else {
      const textSelected = cookies.get("textSelected");
      const splitedTextSelected = textSelected.split(" ");
      const values = {
        userName: cookies.get("userName"),
        durationTime: parseInt(cookies.get("durationTime")) / 60,
        durationTimeSeconds: cookies.get("durationTime"),
        textSelected,
        splitedTextSelected,
        score: 0,
        badScore: 0,
      };
      setInitialValues(values);
      timer.current = setInterval(
        () => setCountDown((v) => v - 1),
        delay * 1000
      );
      return () => {
        clearInterval(timer.current);
      };
    }
  }, []);

  const handleValidateKeyPressed = (e) => {
    if (e.nativeEvent.key === "Backspace") {
      e.stopPropagation();
      e.preventDefault();
    } else if (e.nativeEvent.key === " ") {
      const textTypedSplited = initialValues?.textTyped.split(" ");
      if (
        initialValues.splitedTextSelected[textTypedSplited.length - 1] ===
        textTypedSplited[textTypedSplited.length - 1]
      ) {
        setInitialValues({ ...initialValues, score: initialValues?.score + 1 });
        openSnack("+1 point!");
        if (
          initialValues.splitedTextSelected.length === textTypedSplited.length
        ) {
          handleEndTest(initialValues?.score + 1);
        }
      } else {
        setInitialValues({
          ...initialValues,
          badScore: initialValues?.badScore + 1,
        });
        openSnackErr("Wrong word!");
        if (
          initialValues.splitedTextSelected.length === textTypedSplited.length
        ) {
          handleEndTest();
        }
      }
    }
  };

  const handleEndTest = (score = initialValues?.score) => {
    const finalTime = initialValues?.durationTimeSeconds - countDown;
    const scoreTotal = initialValues?.splitedTextSelected.length;

    cookies.set("score", score);
    cookies.set("badScore", initialValues?.badScore);
    cookies.set("finalTime", finalTime);
    cookies.set("scoreTotal", scoreTotal);
    setOpenModal(true);
  };

  const handleOnChangeText = (e) => {
    setInitialValues({ ...initialValues, textTyped: e.target.value });
  };

  return (
    <>
      <Container style={{ marginTop: "2rem" }}>
        <div>
          <Header as="h2" icon textAlign="center">
            <Header.Content>
              You can do it {initialValues?.userName}
            </Header.Content>
          </Header>
        </div>
        <Grid columns={2} relaxed="very">
          <Grid.Row>
            <Grid.Column verticalAlign="middle">
              <Form.TextArea
                style={{ width: "100%", height: "200px", resize: "none" }}
                label="Text selected:"
                readOnly={true}
                value={initialValues?.textSelected}
              />
            </Grid.Column>
            <Grid.Column verticalAlign="middle">
              <Form.TextArea
                style={{ width: "100%", height: "200px", resize: "none" }}
                label="Your text:"
                onKeyDown={(e) => handleValidateKeyPressed(e)}
                value={initialValues?.textTyped}
                onChange={handleOnChangeText}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid columns={2} relaxed="very">
          <Grid.Row>
            <Grid.Column verticalAlign="middle">
              <div>
                <Header as="h2" icon textAlign="center">
                  <Icon name="gem" circular />
                  <Header.Content>
                    This is your time: {countDown} /{" "}
                    {initialValues?.durationTime} minute(s)
                  </Header.Content>
                  <Child
                    counter={countDown}
                    currentTimer={timer.current}
                    onChange={handleEndTest}
                  />
                </Header>
              </div>
            </Grid.Column>
            <Grid.Column verticalAlign="middle">
              <div>
                <Header as="h2" icon textAlign="center">
                  <Icon name="money" circular />
                  <Header.Content>
                    This is your score: {initialValues?.score}
                  </Header.Content>
                </Header>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
      {openModal && (
        <ModalCountDown
          text={"Your test end in:"}
          redirectUrl={"/challenges/score"}
        />
      )}
    </>
  );
}

function Child({ counter, currentTimer, onChange }) {
  useEffect(() => {
    if (counter > 0) return;
    clearInterval(currentTimer);
    onChange();
  }, [counter, currentTimer]);
  return null;
}
