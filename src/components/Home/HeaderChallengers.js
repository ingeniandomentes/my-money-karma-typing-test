import Link from "next/link";
import { Button, Container, Header, Icon } from "semantic-ui-react";

export const HeaderChallengers = () => {
  return (
    <Container style={{ marginTop: "2rem" }} textAlign="center">
      <div>
        <Header as="h2" icon textAlign="center">
          <Icon name="write" circular />
          <Header.Content>Typing test mymoneykarma</Header.Content>
        </Header>
      </div>
      <Link href="/challenges/new">
        <Button color="green">Be the new Challenger</Button>
      </Link>
    </Container>
  );
};
