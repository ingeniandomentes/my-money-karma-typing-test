import {
  Container,
  Card,
  Image,
  Button,
  Header,
  Icon,
} from "semantic-ui-react";

export const ListChallengers = ({ challenges }) => {
  return (
    <Container style={{ marginTop: "2rem" }}>
      <div>
        <Header as="h2" icon textAlign="center">
          <Icon name="trophy" circular />
          <Header.Content>The last challengers</Header.Content>
        </Header>
      </div>
      <Card.Group>
        {challenges.map((challenge) => (
          <Card key={challenge._id}>
            <Card.Content>
              <Image
                floated="right"
                size="mini"
                src="https://picsum.photos/200"
                alt="profile picture"
              />
              <Card.Header>Name: {challenge.userName}</Card.Header>
              <Card.Meta>Speed: {challenge.speed}</Card.Meta>
              <Card.Description>
                Score:<strong>{challenge.score}</strong>
              </Card.Description>
            </Card.Content>
          </Card>
        ))}
      </Card.Group>
    </Container>
  );
};
